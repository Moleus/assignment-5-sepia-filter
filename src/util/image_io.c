#include <bits/types/FILE.h>
#include "file/file_io.h"
#include "bmp/rw_status_codes.h"
#include "bmp/bmp_reader.h"
#include "util/util.h"
#include "image_io.h"
#include "bmp/bmp_writer.h"

Image read_image_with_destroy(const char* const source_file_name) {
    FILE* source_file;
    Image source_image = {0};

    if (open_file(&source_file, source_file_name, "rb") == OPEN_ERROR) {
        return (struct image) {.valid = false};
    }

    enum read_status result_status = read_bmp_image(&source_image, source_file);
    file_close(source_file);
    if (result_status != READ_OK) {
        destroy_image(source_image);
        println_err(read_error_messages[result_status]);
        println_err("Error occurred while reading source image. Exiting!");
        return (struct image) {.valid = false};
    }
    return source_image;
}


bool write_image_with_destroy(const Image result_image, const char* const output_file_name) {
    if (is_image_invalid(result_image)) {
        destroy_image(result_image);
        println_err("Corrupted image. Exiting!");
        return false;
    }

    FILE* output_file;
    if (open_file(&output_file, output_file_name, "wb") == OPEN_ERROR) {
        destroy_image(result_image);
        return false;
    }

    enum write_status write_image_status = write_bmp_image(result_image, output_file);
    file_close(output_file);

    destroy_image(result_image);
    if (write_image_status != WRITE_OK) {
        println_err(write_error_messages[write_image_status]);
        println_err("Error occurred while writing result image. Exiting!");
        return false;
    }
    return true;
}