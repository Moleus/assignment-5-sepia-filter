#pragma once

#include "format/image.h"

Image read_image_with_destroy(const char* const source_file_name);

bool write_image_with_destroy(const Image result_image, const char* const output_file_name);
