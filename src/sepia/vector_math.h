#pragma once

#include "stdint.h"
#include "format/image.h"

struct color_matrix {
    struct color_effects* b_eff;
    struct color_effects* g_eff;
    struct color_effects* r_eff;
};

struct color_effects {
    double b;
    double g;
    double r;
};

float dot_product(struct pixel p, struct color_effects e);