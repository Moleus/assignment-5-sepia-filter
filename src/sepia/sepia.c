#include "vector_math.h"
#include "format/image.h"

static int saturation_limit(const float value) {
    return value < 255 ? (uint8_t) value : 255;
}

const struct color_matrix sepia_matrix = {
        &((struct color_effects) {.b = 0.131, .g = 0.534, .r = 0.272}),
        &((struct color_effects) {.b = 0.168, .g = 0.686, .r = 0.349}),
        &((struct color_effects) {.b = 0.189, .g = 0.769, .r = 0.393}),
};

static struct pixel apply_pixel(struct pixel pixel) {
    return (struct pixel) {
            .b=saturation_limit(dot_product(pixel, *sepia_matrix.b_eff)),
            .g=saturation_limit(dot_product(pixel, *sepia_matrix.g_eff)),
            .r=saturation_limit(dot_product(pixel, *sepia_matrix.r_eff))
    };
}

Image apply_sepia_filter(const Image source_image) {
    Image target = create_image(source_image.width, source_image.height);
    if (is_image_invalid(target)) return target;

    for (size_t height = 0; height < source_image.height; height++) {
        for (size_t width = 0; width < source_image.width; width++) {
            struct pixel old_pixel = get_pixel(source_image, height, width);
            set_pixel(target, apply_pixel(old_pixel), height, width);
        }
    }
    return target;
}

extern void sepia_filter_asm(struct pixel[static 4], struct pixel * restrict);

Image fast_apply_sepia_filter(Image source_image) {
    Image target = create_image(source_image.width, source_image.height);
    if (is_image_invalid(target)) return target;

    const size_t pixels_count = source_image.width * source_image.height;
    const size_t left_pixels_count = pixels_count % 4;

    for (int i = 0; i < pixels_count / 4; ++i) {
        sepia_filter_asm(4 * i + source_image.pixels, 4 * i + target.pixels);
    }

    for (int i = pixels_count - left_pixels_count; i < left_pixels_count; ++i) {
        target.pixels[i] = apply_pixel(source_image.pixels[i]);
    }
    return target;
}
