; transposed color multipliers
; xmm3 = 0.131 0.168 0.189 0.131 ;  blue
; xmm4 = 0.543 0.686 0.769 0.543 ;  green
; xmm5 = 0.272 0.349 0.393 0.272 ;  red

; pixels
; xmm0 = b1 b1 b1 b2
; xmm1 = g1 g1 g1 g2
; xmm2 = r1 r1 r1 r2

; shuffles
; first:
; xmm0 = b1 b1 b1 b2
; xmm1 = g1 g1 g1 g2
; xmm2 = r1 r1 r1 r2
; second:
; xmm0 = b2 b2 b3 b3
; xmm1 = g2 g2 g3 g3
; xmm2 = r2 r2 r3 r3
; third:
; xmm0 = b3 b4 b4 b4
; xmm1 = g3 g4 g4 g4
; xmm2 = r3 r4 r4 r4

%define SHUFFLE_MULTIPLIERS 0b_01_11_10_01

%macro load_multipliers 0
    movdqa xmm3, [blue_multiplier]
    movdqa xmm4, [green_multiplier]
    movdqa xmm5, [red_multiplier]
%endmacro

%macro shift_multipliers 0
    shufps xmm3, xmm3, SHUFFLE_MULTIPLIERS
    shufps xmm4, xmm4, SHUFFLE_MULTIPLIERS
    shufps xmm5, xmm5, SHUFFLE_MULTIPLIERS
%endmacro

%macro load_pixels 1
    movdqu xmm0, [rdi]   ; b
    movdqu xmm1, [rdi+1] ; g
    movdqu xmm2, [rdi+2] ; r

    pshufb xmm0, %1
    pshufb xmm1, %1
    pshufb xmm2, %1
%endmacro

%macro calculate_sepia 0
    ; to float
    cvtdq2ps xmm0, xmm0
    cvtdq2ps xmm1, xmm1
    cvtdq2ps xmm2, xmm2

    ; multiply by coefficients
    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5

    ; sum of products
    addps xmm0, xmm1
    addps xmm0, xmm2

    ; to double-word integers
    cvtps2dq xmm0, xmm0
    ; pack with saturation
    packssdw xmm0, xmm0
    ; pack to 8 bit
    packuswb xmm0, xmm0
%endmacro

%macro next_pixels 0
    add rdi, 3
    add rsi, 4
%endmacro

%macro store_calculations 0
    movd [rsi], xmm0
%endmacro

%macro process_frame 1
    load_pixels %1
    calculate_sepia
    store_calculations
%endmacro

DEFAULT REL
section .rodata
align 16
blue_multiplier:  dd 0.131, 0.168, 0.189, 0.131
align 16
green_multiplier: dd 0.534, 0.686, 0.769, 0.534
align 16
red_multiplier:   dd 0.272, 0.349, 0.393, 0.272
; x1 x1 x1 x2 with zeroes between
align 16
shuffle_first: db 0x7, 0xFF, 0xFF, 0xFF
               db 0x7, 0xFF, 0xFF, 0xFF
               db 0x7, 0xFF, 0xFF, 0xFF
               db 0x3, 0xFF, 0xFF, 0xFF
; x1 x1 x2 x2 with zeroes between
align 16
shuffle_second: db 0x7, 0xFF, 0xFF, 0xFF
                db 0x7, 0xFF, 0xFF, 0xFF
                db 0x3, 0xFF, 0xFF, 0xFF
                db 0x3, 0xFF, 0xFF, 0xFF
; x1 x2 x2 x2 with zeroes between
align 16
shuffle_third: db 0x7, 0xFF, 0xFF, 0xFF
               db 0x3, 0xFF, 0xFF, 0xFF
               db 0x3, 0xFF, 0xFF, 0xFF
               db 0x3, 0xFF, 0xFF, 0xFF


section .text
global sepia_filter_asm

sepia_filter_asm:
    load_multipliers

    process_frame [shuffle_first]
    next_pixels
    shift_multipliers

    process_frame [shuffle_second]

    next_pixels
    shift_multipliers

    process_frame [shuffle_third]
    ret