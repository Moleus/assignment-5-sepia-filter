#include "vector_math.h"


float dot_product(struct pixel p, struct color_effects e) {
    return p.b * e.b + p.g * e.g + p.r * e.r;
}
