enable_language(ASM_NASM)

file(GLOB_RECURSE sources CONFIGURE_DEPENDS *.c *.h *.asm)

add_executable(image-transformer ${sources})

target_include_directories(image-transformer PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(image-transformer PRIVATE transform_lib)