#include "format/image.h"
#include "sepia/sepia.h"
#include "util/image_io.h"
#include "util/util.h"
#include <bits/types/struct_rusage.h>
#include <stdio.h>
#include <sys/resource.h>


struct timeval count_cpu_time() {
    struct rusage r = {0};
    getrusage(RUSAGE_SELF, &r);
    return r.ru_utime;
}

unsigned long measure_execution_time(struct timeval start, struct timeval end) {
    static uint32_t count = 0;
    static uint64_t differences_sum = 0;
    long difference = (end.tv_sec - start.tv_sec) * 1000000L + end.tv_usec - start.tv_usec;
    differences_sum += difference;
    return differences_sum / ++count;
}

int main(int argc, char** argv) {
    const uint32_t iterations = 100L;
    unsigned long average_execution_time;

    if (argc != 3) {
        println_err("Wrong arguments format; use: `image-transformer <source "
                    "image> <output file>`");
        return 1;
    }

    const char* const source_file_name = argv[1];
    const char* const output_file_name = argv[2];

    const Image source_image = read_image_with_destroy(source_file_name);
    if (is_image_invalid(source_image)) return 1;

    Image sepia_image;
    char* use_assembly = getenv("USE_ASM");

    for (uint32_t i = 0; i < iterations; ++i) {
        struct timeval start = count_cpu_time();
        if (use_assembly) {
            sepia_image = fast_apply_sepia_filter(source_image);
        } else {
            sepia_image = apply_sepia_filter(source_image);
        }
        struct timeval end = count_cpu_time();
        average_execution_time = measure_execution_time(start, end);
    }
    printf("Average time in microseconds: %ld us\n", average_execution_time);

    if (is_image_invalid(source_image)) {
        print_err("Failed to apply sepia filter");
        return 1;
    }
    destroy_image(source_image);

    bool is_ok = write_image_with_destroy(sepia_image, output_file_name);
    if (!is_ok) return 1;

    return 0;
}
