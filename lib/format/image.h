#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

struct __attribute__((packed)) pixel {
  uint8_t b, g, r;
};

typedef struct image {
  size_t width, height;
  bool valid;
  struct pixel *pixels;
} Image;

Image create_image(size_t width, size_t height);

bool is_image_invalid(Image image);

void destroy_image(Image target);

size_t get_size(Image image);

struct pixel get_pixel(Image source, size_t row, size_t col);

void set_pixel(Image target, struct pixel pixel, size_t row, size_t col);
