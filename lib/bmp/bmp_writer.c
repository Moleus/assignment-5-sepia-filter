#include "bmp_writer.h"
#include "bmp_header.h"
#include "bmp_padding.h"
#include "rw_status_codes.h"
#include <bits/types/FILE.h>
#include <stdio.h>

#define FORMAT_TYPE 0x4d42
#define HEADER_SIZE sizeof(struct bmp_header)
#define INFO_HEADER_SIZE 40
#define PLANES_COUNT 1
#define BIT_COUNT 24
#define COMPRESSION 0
#define H_RESOLUTION 2835
#define V_RESOLUTION 2835
#define COLORS_USED 0

static struct bmp_header HEADER_TEMPLATE = {.bfType = FORMAT_TYPE,
                                            .bfReserved = 0,
                                            .bOffBits = HEADER_SIZE,
                                            .biSize = INFO_HEADER_SIZE,
                                            .biPlanes = PLANES_COUNT,
                                            .biBitCount = BIT_COUNT,
                                            .biCompression = COMPRESSION,
                                            .biXPelsPerMeter = H_RESOLUTION,
                                            .biYPelsPerMeter = V_RESOLUTION,
                                            .biClrUsed = COLORS_USED,
                                            .biClrImportant = 0};

static enum write_status write_header(Image image, FILE *file);

static struct bmp_header create_header(Image image);

static enum write_status write_pixels(Image image, FILE *file);

enum write_status write_bmp_image(const Image image, FILE *const file) {
  const enum write_status header_write_status = write_header(image, file);
  if (header_write_status != WRITE_OK) {
    return header_write_status;
  }
  return write_pixels(image, file);
}

static enum write_status write_header(const Image image, FILE *const file) {
  struct bmp_header header = create_header(image);
  if (fwrite(&header, HEADER_SIZE, 1, file) != 1)
    return WRITE_ERR_HEADER;
  return WRITE_OK;
}

static struct bmp_header create_header(const Image image) {
  struct bmp_header header = HEADER_TEMPLATE;
  header.biSizeImage = get_size(image);
  header.bfileSize = header.biSizeImage + HEADER_SIZE;
  header.biWidth = image.width;
  header.biHeight = image.height;
  return header;
}

static enum write_status write_pixels(const Image image, FILE *const file) {
  size_t image_width = image.width;
  size_t pixel_size = sizeof(struct pixel);
  uint8_t padding_length = get_padding(image_width);

  uint32_t zero_on_stack = 0;

  for (size_t i = 0; i < image.height; i++) {
    const void *pixel_position = image.pixels + image_width * i;
    if (fwrite(pixel_position, pixel_size, image_width, file) != image_width) {
      return WRITE_ERR_PIXELS;
    }

    if (fwrite(&zero_on_stack, 1, padding_length, file) != padding_length) {
      return WRITE_ERR_PADDING;
    }
  }
  return WRITE_OK;
}
