#pragma once

#include <stdint.h>

uint8_t get_padding(uint32_t image_width);
