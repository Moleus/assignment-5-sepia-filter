#pragma once

#include "../format/image.h"
#include "rw_status_codes.h"
#include <stdio.h>

enum write_status write_bmp_image(Image image, FILE *file);
