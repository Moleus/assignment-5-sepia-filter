#include "file_io.h"
#include "../util/util.h"
#include <errno.h>
#include <stdio.h>
#include <string.h>

enum open_status open_file(FILE **result_file, const char *file_name,
                           const char *mode) {
  *result_file = fopen(file_name, mode);
  if (*result_file != NULL)
    return OPEN_OK;
  const char *error_desc = strerror(errno);
  print_err("Error occurred while opening a file: ");
  println_err(error_desc);
  return OPEN_ERROR;
}

enum close_status file_close(FILE *file) {
  int res = fclose(file);
  if (res == 0)
    return CLOSE_OK;
  const char *error_desc = strerror(errno);
  print_err("Error occurred while closing a file: ");
  println_err(error_desc);
  return CLOSE_ERROR;
}
