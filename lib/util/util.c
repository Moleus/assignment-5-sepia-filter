#include <stdio.h>

void print_err(const char *err_message) { fprintf(stderr, "%s", err_message); }

void println_err(const char *err_message) {
  fprintf(stderr, "%s\n", err_message);
}

void println(const char *message) { fprintf(stdout, "%s\n", message); }

void print(const char *message) { fprintf(stdout, "%s", message); }
